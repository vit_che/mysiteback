<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Cors
{

    public function handle(Request $request, Closure $next)
    {

//        if ($request->isMethod('OPTIONS')) {
//
//            return $next($request)->header('Access-Control-Allow-Origin', 'http://localhost:3000')
//                ->header('Access-Control-Allow-Methods', 'POST, GET');
//        }
//
//            return $next($request)
//            ->header('Access-Control-Allow-Headers', 'accept, content-type, Origin, Content-Type, Autorization')
//            ->header('Access-Control-Allow-Origin', '*')
//            ->header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE, PUT, PATCH');


        return $next($request)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
            ->header('Access-Control-Expose-Headers', 'Authorization')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }
}




















//        return $next($request)
//            ->header('Access-Control-Allow-Origin', '*')
//            ->header('Access-Control-Allow-Headers', 'accept, content-type, Origin, Content-Type, Autorization')
//            ->header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE, PUT, PATCH')
//            ->header('Access-Control-Allow-Methods', 'OPTIONS')
//            ->header('Access-Control-Allow-Methods', 'GET')
//            ->header('Access-Control-Allow-Methods', 'POST');

//                ->header('Access-Control-Allow-Methods', 'Content-Type, Authorization');
