<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['cors','web']], function () {

        Route::get('/getComments', ['uses' => 'CommentController@getComments', 'as' => 'getComments']);

        Route::match(['post', 'get'], '/addComment', ['uses' => 'CommentController@addComment', 'as' => 'addComment']);
});

Route::options('{any}', ['middleware' => ['cors'], function () {

    return response(['status' => 'success']);

}])->where('any', '.*');

