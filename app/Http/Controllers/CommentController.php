<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use DB;

class CommentController extends Controller
{
    public function getComments(){

        $comments = DB::table('comments')
//            ->join('users', 'comments.user_id', '=', 'users.id')
//            ->select('comments.id as id', 'title', 'text', 'users.name as name')
            ->select('comments.id as id','title', 'text')
            ->get();

        return $comments;
    }

    public function addComment(Request $request){

        $input = $request->except('_token');

        $comment = new Comment();

        $comment->fill($input);

        if ($comment->save()) {

            $message = "Comment was saved!";

            return $message;

        } else {

            $message = " Error saved!";

            return $message;
        }

    }
}
